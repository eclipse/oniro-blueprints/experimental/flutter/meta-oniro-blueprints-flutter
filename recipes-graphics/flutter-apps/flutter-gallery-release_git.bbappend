# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: MIT


INSANE_SKIP:${PN} += " dev-so"

#FLUTTER_APPLICATION_INSTALL_PREFIX ??= ""
#PUBSPEC_APPNAME ??= "gallery"
#FLUTTER_INSTALL_DIR = "${datadir}${FLUTTER_APPLICATION_INSTALL_PREFIX}/${PUBSPEC_APPNAME}"

do_install:append() {
        install -d ${D}${FLUTTER_INSTALL_DIR}/flutter_assets/
	ln -s ../lib/libapp.so ${D}${FLUTTER_INSTALL_DIR}/flutter_assets/
}

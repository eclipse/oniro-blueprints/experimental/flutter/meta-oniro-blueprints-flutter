# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: MIT

require blueprint-flutter-image_0.bb

IMAGE_INSTALL:append = "\
        gdb \
        perf \
        screen \
        strace \
        zile \
    "

EXTRA_IMAGE_FEATURES += "debug-tweaks"

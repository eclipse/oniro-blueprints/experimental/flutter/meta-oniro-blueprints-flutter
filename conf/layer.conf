# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: MIT

# We have a conf and classes directory, add to BBPATH
BBPATH .= ":${LAYERDIR}"

# We have recipes-* directories, add to BBFILES
BBFILES += "${LAYERDIR}/recipes-*/*/*.bb \
            ${LAYERDIR}/recipes-*/*/*.bbappend"

BBFILE_COLLECTIONS += "meta-oniro-blueprint-flutter"
BBFILE_PATTERN_meta-oniro-blueprint-flutter = "^${LAYERDIR}/"
BBFILE_PRIORITY_meta-oniro-blueprint-flutter = "6"

LAYERDEPENDS_meta-oniro-blueprint-flutter = " \
    core \
    openembedded-layer \
    oniro-core \
    oniro-staging \
    meta-flutter \
"

LAYERSERIES_COMPAT_meta-oniro-blueprint-flutter = "dunfell honister kirkstone"

# Pin latest tag: https://github.com/meta-flutter/meta-flutter/pull/71
FLUTTER_SDK_TAG ?= "3.0.1"
# FLUTTER_CHANNEL ?= "stable"
FLUTTER_RUNTIME ?= "release"

# TODO: Pin latest version
IVI_HOMESCREEN_SRCREV ?= "35cf0e2549cf1954afc3648f96c93d857e982a20"
SRCREV:pn-ivi-homescreen-release ?= "${IVI_HOMESCREEN_SRCREV}"
SRCREV:pn-ivi-homescreen-debug ?= "${IVI_HOMESCREEN_SRCREV}"
SRCREV:pn-ivi-homescreen-profile ?= "${IVI_HOMESCREEN_SRCREV}"

PACKAGECONFIG:pn-flutter-engine-release = "disable-desktop-embeddings embedder-for-target fontconfig release"
PACKAGECONFIG:pn-flutter-engine-debug = "disable-desktop-embeddings embedder-for-target fontconfig debug"
PACKAGECONFIG:pn-flutter-engine-profile = "disable-desktop-embeddings embedder-for-target fontconfig profile"

IVI_HOMESCREEN_SERVICE_EXEC_START_PARAMS = "--b=/usr/share/flutter/gallery --t=DMZ-White"
SERVICE_EXEC_START_PARAMS:pn-ivi-homescreen-debug = "${IVI_HOMESCREEN_SERVICE_EXEC_START_PARAMS}"
SERVICE_EXEC_START_PARAMS:pn-ivi-homescreen-profile = "${IVI_HOMESCREEN_SERVICE_EXEC_START_PARAMS}"
SERVICE_EXEC_START_PARAMS:pn-ivi-homescreen-release = "${IVI_HOMESCREEN_SERVICE_EXEC_START_PARAMS}"

IVI_HOMESCREEN_SERVICE_UNIT = "After=weston.service\n"
SERVICE_UNIT:pn-ivi-homescreen-debug = "${IVI_HOMESCREEN_SERVICE_UNIT}"
SERVICE_UNIT:pn-ivi-homescreen-profile = "${IVI_HOMESCREEN_SERVICE_UNIT}"
SERVICE_UNIT:pn-ivi-homescreen-release = "${IVI_HOMESCREEN_SERVICE_UNIT}"

IVI_HOMESCREEN_SERVICE_USER_GROUP = "User=weston\nGroup=weston"
SERVICE_USER_GROUP:pn-ivi-homescreen-debug = "${IVI_HOMESCREEN_SERVICE_USER_GROUP}"
SERVICE_USER_GROUP:pn-ivi-homescreen-profile = "${IVI_HOMESCREEN_SERVICE_USER_GROUP}"
SERVICE_USER_GROUP:pn-ivi-homescreen-release = "${IVI_HOMESCREEN_SERVICE_USER_GROUP}"

IVI_HOMESCREEN_SERVICE_ENVIRONMENT_WAYLAND_DISPLAY:seco-px30-d23 ?= 'wayland-0'
IVI_HOMESCREEN_SERVICE_ENVIRONMENT_WAYLAND_DISPLAY ?= 'wayland-1'

IVI_HOMESCREEN_SERVICE_ENVIRONMENT = "Environment=HOME=/tmp\nEnvironment=XDG_RUNTIME_DIR=/run/user/1000\nEnvironment=WAYLAND_DISPLAY=${IVI_HOMESCREEN_SERVICE_ENVIRONMENT_WAYLAND_DISPLAY}"
SERVICE_ENVIRONMENT:pn-ivi-homescreen-debug = "${IVI_HOMESCREEN_SERVICE_ENVIRONMENT}"
SERVICE_ENVIRONMENT:pn-ivi-homescreen-profile = "${IVI_HOMESCREEN_SERVICE_ENVIRONMENT}"
SERVICE_ENVIRONMENT:pn-ivi-homescreen-release = "${IVI_HOMESCREEN_SERVICE_ENVIRONMENT}"

IVI_HOMESCREEN_SERVICE_INSTALL = "WantedBy=multi-user.target\n"
SERVICE_INSTALL:pn-ivi-homescreen-debug = "${IVI_HOMESCREEN_SERVICE_INSTALL}"
SERVICE_INSTALL:pn-ivi-homescreen-profile = "${IVI_HOMESCREEN_SERVICE_INSTALL}"
SERVICE_INSTALL:pn-ivi-homescreen-release = "${IVI_HOMESCREEN_SERVICE_INSTALL}"

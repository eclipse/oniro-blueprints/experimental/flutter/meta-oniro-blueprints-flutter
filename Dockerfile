# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: MIT

FROM debian:11
LABEL maintainer="Philippe Coval <philippe.coval@huawei.com>"

ENV DEBIAN_FRONTEND noninteractive
# Your system needs to support the en_US.UTF-8 locale.
ENV LC_ALL en_US.UTF-8
ENV LANG ${LC_ALL}

RUN echo "# info: Configuring locale (${LC_ALL} s{LANG})" \
  && set -x \
  && apt-get update -y \
  && apt-get install -y \
       --no-install-recommends \
       locales \
  && echo "${LC_ALL} UTF-8" | tee /etc/locale.gen \
  && locale-gen ${LC_ALL} \
  && dpkg-reconfigure locales \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* \
  && date -u

RUN echo "# info: Setup system for OE/Yocto bitbake" \
  && set -x \
  && echo ${SHELL} \
  && apt-get update -y \
  && apt-get install -y \
       --no-install-recommends \
       bzip2 \
       cpp \
       g++ \
       gcc \
       make \
       patch \
       xz-utils \
  && apt-get install -y \
       --no-install-recommends \
       build-essential \
       debianutils \
  && apt-get install -y \
       --no-install-recommends \
       git \
       chrpath \
       cpio \
       diffstat \
       file \
       gawk \
       wget \
  && apt-get install -y \
       --no-install-recommends \
       gcc-multilib \
       iputils-ping \
       libegl1-mesa \
       libsdl1.2-dev \
       pylint3 \
       python3 \
       python3-git \
       python3-jinja2 \
       python3-pexpect \
       python3-pip \
       socat \
       texinfo \
       unzip \
       xterm \
       lz4 \
       zstd \
  && apt-get install -y \
       --no-install-recommends \
       ssh \
  && echo "info: repo is included in contrib because it downloads code via git" \
  && sed -e 's|deb \(.*\) main$|deb \1 main contrib|g' -i /etc/apt/*.list \
  && apt-get update -y \
  && apt-get install -y --no-install-recommends repo \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* \
  && date -u

ARG blueprint=flutter
ENV os=oniro
ENV project oniro-blueprint-${blueprint}
ENV workdir "/local/src/${project}"
ARG recipe=blueprint-${blueprint}-image
ARG flavour=linux
ARG distro=${os}-${flavour}-blueprint-${blueprint}
ARG templateconf="${workdir}/${os}/flavours/${flavour}"
ENV user ${project}
COPY . "${workdir}/"
RUN echo "# log: ${project}: Setup user ${user}" \
  && set -x \
  && useradd -ms /bin/bash "${project}" \
  && chown -R "${user}" "${workdir}" \
  && date -u

USER "${user}"
WORKDIR "${workdir}"
RUN echo "# info: ${project}: Download sources" \
  && set -x \
  && repo init "file://${workdir}/" \
  && repo sync \
  && date -u

WORKDIR "${workdir}"
USER "${user}"
RUN echo "# info: ${project}: Build ${recipe}" \
  && set -x \
  && du -msc * \
  && export DISTRO="${distro}" \
  && export TEMPLATECONF="${templateconf}" \
  && cd oe-core && . ./oe-init-build-env \
  && bitbake-layers add-layer ${workdir}/meta-${blueprint} \
  && bitbake-layers add-layer ${workdir}/. \
  && bitbake ${recipe} \
  && date -u
